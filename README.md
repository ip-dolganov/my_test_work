# my_test_work

## Установка и запуск:
1. Установите [Docker](https://docs.docker.com/v17.09/engine/installation/)
2. Перейдите в папку проекта
3. Выполните:

```bash
docker-compose up -d
```

Сайт будет доступен по адресу 127.0.0.1:8000

## Авторизация

Для авторизации используется механизм [DRF authtoken](https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication)

Каждый авторизованный запрос должен содержать HTTP Header

_HTTP_AUTHORIZATION: Token {token}_

Token вы можете получить, отправив POST запрос на эндпоинт

_/api/auth/sign-in_

Принимает параметры в теле (body) запроса:
- username
- password

## URL APIAppViewSet

Для доступа к APIApp View используйте эндпоинт

_/api/api-apps/_

Эндпоинт реализует [DRF GenericView](https://www.django-rest-framework.org/api-guide/routers/#simplerouter) и поддерживает операции:

- List: GET _/api/api-apps/_
- Retrieve: GET _/api/api-apps/{app_id}/_
- Create: POST _/api/api-apps/{app_id}/_ (поле name - str)
- Update: PUT _/api/api-apps/{app_id}/_ (поле name - str)
- Partial Update: PATCH _/api/api-apps/{app_id}/_ (поле name - str)
- Destroy: DELETE _/api/api-apps/{app_id}/_

Для того, чтобы поменять key приложения, используйте эндпоинт

GET _/api/api-apps/{app_id}/regenerate-key/_

## URL MyAPIAppView

Данный эндпоинт возвращает то же самое, что и APIAppViewSet Retrieve, но не требует передачи
ID api_app в адресной строке. Вместо этого, выдолжны установить HTTP HEADER

_HTTP_API_APP_KEY: {api_app.key}_

Используется запрос:

GET _/api/test_

## Выход из системы

Для выхода из системы используйте

GET _/api/auth/log-out_