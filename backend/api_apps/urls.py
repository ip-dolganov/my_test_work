from rest_framework.routers import DefaultRouter
from api_apps.views.api_app.views import APIAppViewSet

from django.urls import re_path

router = DefaultRouter()
router.register(
    r'',
    APIAppViewSet
)

urlpatterns = router.urls
