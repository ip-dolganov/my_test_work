from django.test import TestCase
from api_apps.factories.api_app import APIAppFactory
from django.apps import apps

APIApp = apps.get_model('api_apps', 'APIApp')


class TestAPIAPPModel(TestCase):
    """
    Test APIApp Model
    """

    def setUp(self) -> None:
        self.api_app: APIApp = APIAppFactory()

    def test_regenerate_key(self):
        old_key: str = str(
            self.api_app.key
        )
        self.api_app.regenerate_key()
        self.assertNotEqual(
            old_key,
            str(
                self.api_app.key
            )
        )
