from rest_framework.test import APITestCase
from application.factories.user import UserFactory
from application.utils.tests.get_auth_token_mixin import GetAuthTokenMixin
from django.urls import reverse_lazy
from rest_framework import status
from api_apps.factories.api_app import APIAppFactory


class TestAPIAppViewSetRegenerateKey(
    GetAuthTokenMixin,
    APITestCase
):
    """
    Test api_apps.views.api_app.views.APIAppViewSet regenerate_key
    """

    def setUp(self) -> None:
        self.user = UserFactory()
        self.api_app = APIAppFactory()
        self.url = reverse_lazy(
            'api-apps:apiapp-regenerate-key',
            kwargs={
                'pk': self.api_app.pk
            }
        )

    def test_anonymously(self):
        response = self.client.get(self.url)
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            response.status_code
        )

    def test_authenticated(self):
        auth_client = self.get_auth_client()
        response = auth_client.get(self.url)
        self.assertEqual(
            status.HTTP_200_OK,
            response.status_code
        )

    def test_key_is_new(self):
        auth_client = self.get_auth_client()
        old_key = self.api_app.key
        response = auth_client.get(self.url)
        self.assertNotEqual(
            old_key,
            response.data['key']
        )
