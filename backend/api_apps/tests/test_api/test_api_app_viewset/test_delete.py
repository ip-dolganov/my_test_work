from rest_framework.test import APITestCase
from application.factories.user import UserFactory
from application.utils.tests.get_auth_token_mixin import GetAuthTokenMixin
from django.urls import reverse_lazy
from rest_framework import status
from api_apps.factories.api_app import APIAppFactory


class TestAPIAppViewSetDelete(
    GetAuthTokenMixin,
    APITestCase
):
    """
    Test api_apps.views.api_app.views.APIAppViewSet delete
    """

    def setUp(self) -> None:
        self.user = UserFactory()
        api_app = APIAppFactory()
        self.url = reverse_lazy(
            'api-apps:apiapp-detail',
            kwargs={
                'pk': api_app.pk
            }
        )

    def test_anonymously(self):
        response = self.client.delete(self.url)
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            response.status_code
        )

    def test_authenticated(self):
        auth_client = self.get_auth_client()
        response = auth_client.delete(self.url)
        self.assertEqual(
            status.HTTP_204_NO_CONTENT,
            response.status_code
        )
