from rest_framework.test import APITestCase
from application.factories.user import UserFactory
from application.utils.tests.get_auth_token_mixin import GetAuthTokenMixin
from django.urls import reverse_lazy
from rest_framework import status


class TestAPIAppViewSetCreate(
    GetAuthTokenMixin,
    APITestCase
):
    """
    Test api_apps.views.api_app.views.APIAppViewSet create
    """

    url = reverse_lazy('api-apps:apiapp-list')

    def setUp(self) -> None:
        self.user = UserFactory()
        self.data = {
            'name': 'my app'
        }

    def test_anonymously(self):
        response = self.client.get(self.url)
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            response.status_code
        )

    def test_authenticated(self):
        auth_client = self.get_auth_client()
        response = auth_client.post(
            self.url,
            data=self.data
        )
        self.assertEqual(
            status.HTTP_201_CREATED,
            response.status_code
        )
