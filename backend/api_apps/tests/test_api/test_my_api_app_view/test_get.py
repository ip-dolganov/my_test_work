from rest_framework.test import APITestCase
from application.factories.user import UserFactory
from application.utils.tests.get_auth_token_mixin import GetAuthTokenMixin
from django.urls import reverse_lazy
from rest_framework import status
from api_apps.factories.api_app import APIAppFactory


class TestMyAPIAppView(
    GetAuthTokenMixin,
    APITestCase
):
    """
    Test api_apps.views.app_related.views.MyAPIAppView
    """

    url = reverse_lazy('test')

    def setUp(self) -> None:
        self.user = UserFactory()
        self.api_app = APIAppFactory()

    def test_anonymously(self):
        response = self.client.get(self.url)
        self.assertEqual(
            status.HTTP_401_UNAUTHORIZED,
            response.status_code
        )

    def test_authenticated(self):
        auth_client = self.get_auth_client()
        response = auth_client.get(self.url)
        self.assertEqual(
            status.HTTP_403_FORBIDDEN,
            response.status_code
        )

    def test_with_key(self):
        token = self.get_token()
        auth_client = self.get_auth_client()
        auth_client.credentials(
            HTTP_API_APP_KEY=self.api_app.key,
            HTTP_AUTHORIZATION='Token ' + token
        )
        response = auth_client.get(self.url)
        self.assertEqual(
            status.HTTP_200_OK,
            response.status_code
        )
