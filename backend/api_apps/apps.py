from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class APIAppsConfig(AppConfig):
    """
    Custom API Apps config
    """
    name = 'api_apps'
    verbose_name = _('API apps')
