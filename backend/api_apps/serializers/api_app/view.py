from rest_framework import serializers
from django.apps import apps

APIApp = apps.get_model('api_apps', 'APIApp')


class APIAppViewSerializer(serializers.ModelSerializer):
    """
    Serialize APIApp to view
    """

    class Meta:
        model = APIApp
        fields = (
            'id',
            'name',
            'key',
            'created_at'
        )
