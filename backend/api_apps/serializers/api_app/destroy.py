from rest_framework import serializers
from django.apps import apps

APIApp = apps.get_model('api_apps', 'APIApp')


class APIAppDestroySerializer(serializers.ModelSerializer):
    """
    Serialize APIApp to destroy
    """

    class Meta:
        model = APIApp
        fields = (
            'id',
        )
