from rest_framework import serializers
from django.apps import apps
from api_apps.serializers.api_app.view import APIAppViewSerializer

APIApp = apps.get_model('api_apps', 'APIApp')


class APIAppWriteSerializer(serializers.ModelSerializer):
    """
    Serialize APIApp to create/update
    """

    @property
    def data(self):
        serializer = APIAppViewSerializer(self.instance)
        return serializer.data

    class Meta:
        model = APIApp
        fields = (
            'name',
        )
