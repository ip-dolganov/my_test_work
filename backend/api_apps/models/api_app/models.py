from django.db import models
from django.utils.translation import ugettext_lazy as _
from uuid import uuid4
from typing import NoReturn


class APIApp(models.Model):
    """
    API Application
    """
    key = models.UUIDField(
        verbose_name=_('uuid'),
        blank=True,
        default=uuid4,
        unique=True
    )
    name = models.CharField(
        max_length=255,
        verbose_name=_('name'),
        unique=True
    )
    created_at = models.DateTimeField(
        verbose_name=_('created at'),
        auto_now_add=True,
        blank=True
    )

    def regenerate_key(self) -> NoReturn:
        self.key: str = uuid4().hex
        self.save()

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = _('API app')
        verbose_name_plural = _('API apps')
        ordering = (
            '-created_at',
            '-id'
        )


__all__ = ['APIApp']
 