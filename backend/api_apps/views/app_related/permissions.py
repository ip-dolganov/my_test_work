from rest_framework.permissions import BasePermission


class APIAppHeaderIsSet(BasePermission):
    """
    Checks, that HTTP_API_APP_KEY is set
    """

    def has_permission(self, request, view):
        api_app = getattr(
            request,
            'api_app',
            None
        )
        return api_app is not None
