from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from api_apps.views.app_related.permissions import APIAppHeaderIsSet
from api_apps.serializers.api_app.view import APIAppViewSerializer
from django.apps import apps


APIApp = apps.get_model('api_apps', 'APIApp')


class MyAPIAppView(APIView):
    """
    View my API app
    """

    permission_classes = [
        IsAuthenticated,
        APIAppHeaderIsSet
    ]

    def get(
            self,
            request,
            *args,
            **kwargs
    ) -> Response:
        serializer_ = APIAppViewSerializer(request.api_app)
        return Response(
            serializer_.data
        )
