from rest_framework import permissions, mixins, viewsets
from django.apps import apps
from api_apps.serializers.api_app.view import APIAppViewSerializer
from api_apps.serializers.api_app.write import APIAppWriteSerializer
from api_apps.serializers.api_app.destroy import APIAppDestroySerializer
from rest_framework.decorators import action
from rest_framework.response import Response

APIApp = apps.get_model('api_apps', 'APIApp')


class APIAppViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet
):
    """
    APIApp model ViewSet
    """

    serializer_classes = {
        'list': APIAppViewSerializer,
        'retrieve': APIAppViewSerializer,
        'create': APIAppWriteSerializer,
        'update': APIAppWriteSerializer,
        'partial_update': APIAppWriteSerializer,
        'destroy': APIAppDestroySerializer,
        'regenerate_key': APIAppViewSerializer,
    }
    permissions_map = {
        'list': [permissions.IsAuthenticated],
        'retrieve': [permissions.IsAuthenticated],
        'create': [permissions.IsAuthenticated],
        'update': [permissions.IsAuthenticated],
        'partial_update': [permissions.IsAuthenticated],
        'destroy': [permissions.IsAuthenticated],
        'regenerate_key': [permissions.IsAuthenticated],
    }

    queryset = APIApp.objects.all()

    def get_serializer_class(self):
        return self.serializer_classes.get(
            self.action,
            self.serializer_classes['list']
        )

    def get_permissions(self):
        permissions_ = self.permissions_map.get(
            self.action,
            [permissions.IsAuthenticated]
        )
        return [permission() for permission in permissions_]

    @action(
        methods=['get'],
        detail=True,
        url_path='regenerate-key',
        url_name='regenerate-key'
    )
    def regenerate_key(self, request, *args, **kwargs):
        instance: APIApp = self.get_object()
        instance.regenerate_key()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
