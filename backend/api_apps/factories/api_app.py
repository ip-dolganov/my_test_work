import factory
from django.apps import apps

APIApp = apps.get_model('api_apps', 'APIApp')


class APIAppFactory(factory.DjangoModelFactory):
    """
    APIApp model factory
    """

    name = 'My App'

    class Meta:
        model = APIApp
        django_get_or_create = (
            'name',
        )
