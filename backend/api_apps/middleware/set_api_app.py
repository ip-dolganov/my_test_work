from django.apps import apps
from django.http import HttpRequest
from typing import Optional
from django.db.models import QuerySet
from uuid import UUID

APIApp = apps.get_model('api_apps', 'APIApp')


def get_api_app_key(
        request: HttpRequest
) -> Optional[UUID]:
    key = request.META.get('HTTP_API_APP_KEY', None)
    if isinstance(key, str):
        key = UUID(key)
    return key


def get_api_app_by_key(
        key: UUID
) -> Optional[APIApp]:
    api_app_qs: QuerySet = APIApp.objects.filter(
        key=key
    )
    return api_app_qs.first()


class SetAPIAppMiddleware:
    """
    Sets up request.user.api_app attribute
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request = self.__set_up_api_app(request)
        response = self.get_response(request)
        return response

    def __set_up_api_app(self, request) -> HttpRequest:
        if hasattr(request, 'user'):
            api_app_key = get_api_app_key(request)
            api_app = get_api_app_by_key(api_app_key)
            request.api_app = api_app
        return request
