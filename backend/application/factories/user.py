import factory
from django.contrib.auth import get_user_model


User = get_user_model()


class UserFactory(factory.DjangoModelFactory):
    """
    User model factory
    """

    username = 'test user'
    password = 'qwerty'

    class Meta:
        model = User
        django_get_or_create = (
            'username',
        )

    @classmethod
    def _get_or_create(cls, model_class, *args, **kwargs):
        password = kwargs.pop('password')
        instance = super(UserFactory, cls)._get_or_create(model_class, *args, **kwargs)
        instance.set_password(password)
        instance.save()
        return instance
