from rest_framework.test import APITestCase
from application.factories.user import UserFactory
from django.urls import reverse_lazy
from rest_framework import status
from typing import Dict
from rest_framework.response import Response


class TestSignInView(
    APITestCase
):
    """
    Test sign-in url
    """

    url = reverse_lazy('auth:sign-in')

    def setUp(self) -> None:
        self.user = UserFactory()

    def __get_response(self) -> Response:
        credentials: Dict = {
            'username': 'test user',
            'password': 'qwerty'
        }
        response: Response = self.client.post(
            self.url,
            data=credentials
        )
        return response

    def test_status(self):
        response = self.__get_response()
        self.assertEqual(
            status.HTTP_200_OK,
            response.status_code
        )

    def test_user_has_token(self):
        self.__get_response()
        user_token = getattr(
            self.user,
            'auth_token',
            None
        )
        self.assertIsNotNone(
            user_token
        )

    def test_token_in_response(self):
        response = self.__get_response()
        self.assertIn(
            'token',
            response.data.keys()
        )

    def test_token_is_valid(self):
        response = self.__get_response()
        self.assertEqual(
            self.user.auth_token.key,
            response.data['token']
        )
