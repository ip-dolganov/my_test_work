from rest_framework.test import APITestCase
from application.factories.user import UserFactory
from application.utils.tests.get_auth_token_mixin import GetAuthTokenMixin
from django.urls import reverse_lazy
from rest_framework import status


class TestLogOutView(
    GetAuthTokenMixin,
    APITestCase
):
    """
    Test application.views.log_out.LogOutView
    """

    log_out_url = reverse_lazy('auth:log-out')

    def setUp(self) -> None:
        self.user = UserFactory()

    def test_status(self):
        auth_client = self.get_auth_client()
        response = auth_client.get(
            self.log_out_url
        )
        self.assertEqual(
            status.HTTP_200_OK,
            response.status_code
        )

    def test_user_has_no_token(self):
        auth_client = self.get_auth_client()
        auth_client.get(
            self.log_out_url
        )
        user_token = getattr(
            self.user,
            'auth_token',
            None
        )
        self.assertIsNone(
            user_token
        )
