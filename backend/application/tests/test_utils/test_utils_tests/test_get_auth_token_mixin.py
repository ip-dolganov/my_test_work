from rest_framework.test import APITestCase
from application.utils.tests.get_auth_token_mixin import GetAuthTokenMixin
from application.factories.user import UserFactory


class TestGetAuthTokenMixin(
    GetAuthTokenMixin,
    APITestCase
):
    """
    Test GetAuthTokenMixin
    """

    def setUp(self) -> None:
        UserFactory()

    def test_get_token(self):
        token = self.get_token()
        self.assertIsNotNone(
            token
        )
