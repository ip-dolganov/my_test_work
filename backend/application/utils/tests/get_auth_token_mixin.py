from django.urls import reverse_lazy
from copy import deepcopy
from rest_framework.test import APIClient
from typing import Dict, Optional


class GetAuthTokenMixin:
    """
    Get auth token mixin
    """

    sign_in_url = reverse_lazy('auth:sign-in')

    def get_token(
            self,
            username: str = 'test user',
            password: str = 'qwerty'
    ) -> Optional[str]:
        credentials: Dict = {
            'username': username,
            'password': password
        }
        response = self.client.post(
            self.sign_in_url,
            data=credentials
        )
        return response.data.get('token', None)

    def get_auth_client(
            self,
            username: str = 'test user',
            password: str = 'qwerty'
    ) -> APIClient:
        auth_client: APIClient = deepcopy(self.client)
        token = self.get_token(
            username,
            password
        )
        auth_client.credentials(
            HTTP_AUTHORIZATION='Token ' + token
        )
        return auth_client