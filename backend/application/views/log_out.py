from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from rest_framework.response import Response
from rest_framework.views import APIView


class LogOutView(APIView):
    """
    Logout read (REST)
    """

    permission_classes = []

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            if hasattr(request.user, 'auth_token') and request.user.auth_token:
                request.user.auth_token.delete()
            logout(request)
        if request.GET.get('redirect_uri'):
            return HttpResponseRedirect(request.GET['redirect_uri'])
        else:
            return Response({'logout': True})
