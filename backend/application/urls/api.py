from django.urls import re_path, include
from api_apps.views.app_related.views import MyAPIAppView

api_urls = [
    re_path(
        r'^api/auth/',
        include(
            (
                'application.urls.auth',
                'auth'
            ),
            namespace='auth'
        )
    ),
    re_path(
        r'^api/api-apps/',
        include(
            (
                'api_apps.urls',
                'api-apps'
            ),
            namespace='api-apps'
        )
    ),
    re_path(
        r'^api/test/',
        MyAPIAppView.as_view(),
        name='test'
    ),
]
