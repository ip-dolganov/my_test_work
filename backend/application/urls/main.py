from django.urls import re_path
from django.conf import settings
from django.contrib.staticfiles import views
from django.conf.urls.static import static
from application.urls.system import system_urls
from application.urls.api import api_urls

urlpatterns = system_urls + api_urls

if settings.DEBUG:
    urlpatterns += static(
        settings.MEDIA_URL,
        document_root=settings.MEDIA_ROOT
    )
    urlpatterns += [
        re_path(
            r'^static/(?P<path>.*)$',
            views.serve
        ),
    ]
