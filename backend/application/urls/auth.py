from rest_framework.authtoken.views import ObtainAuthToken
from django.urls import re_path
from application.views.log_out import LogOutView

urlpatterns = [
    re_path(
        r'sign-in',
        ObtainAuthToken.as_view(),
        name='sign-in'
    ),
    re_path(
        r'log-out',
        LogOutView.as_view(),
        name='log-out'
    ),
]
