from django.urls import re_path
from django.contrib import admin

system_urls = [
    re_path(
        r'^admin',
        admin.site.urls
    ),
]