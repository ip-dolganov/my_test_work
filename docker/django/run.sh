#!/usr/bin/env bash
python manage.py check --deploy --fail-level 'WARNING'
python manage.py migrate --noinput
python manage.py collectstatic --noinput --clear

# Start gunicorn with 2 workers:
/usr/local/bin/gunicorn  application.wsgi -c 'python:application.gunicorn_conf'